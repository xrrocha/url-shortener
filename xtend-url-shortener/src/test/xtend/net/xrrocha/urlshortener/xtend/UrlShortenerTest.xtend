package net.xrrocha.urlshortener.xtend

import java.io.Closeable
import java.io.IOException
import net.xrrocha.urlshortener.xtend.shortening.Shortener
import net.xrrocha.urlshortener.xtend.storage.StorageProvider
import org.junit.Test

import static org.mockito.Mockito.*

/**
 * URL shortener unit tests.
 */
class UrlShortenerTest {
  @Test(expected = IllegalArgumentException)
  def void validationFailsOnNullApiPath() {
    UrlShortener.builder.
      apiPath(null).
      redirectPath('/').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnEmptyApiPath() {
    UrlShortener.builder.
      apiPath('').
      redirectPath('/').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnBlankApiPath() {
    UrlShortener.builder.
    apiPath(' \t\r\n').
    redirectPath('/').
    shortener(mock(Shortener)).
    storageProvider(mock(StorageProvider)).
    build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnNoSlashApiPath() {
    UrlShortener.builder.
      apiPath('api/shorten').
      redirectPath('/').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnNullRedirectPath() {
    UrlShortener.builder.
        apiPath('/api/shorten').
        redirectPath(null).
        shortener(mock(Shortener)).
        storageProvider(mock(StorageProvider)).
        build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnEmptyRedirectPath() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnBlankRedirectPath() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath(' \t\r\n').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnNoHeadingSlashRedirectPath() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('redirect/').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void validationFailsOnNoTrailingSlashRedirectPath() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('/redirect').
      shortener(mock(Shortener)).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = NullPointerException)
  def void validationFailsOnNullUrlShortener() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('/redirect/').
      shortener(null).
      storageProvider(mock(StorageProvider)).
      build
  }

  @Test(expected = NullPointerException)
  def void validationFailsOnNullStorageProvider() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('/redirect/').
      shortener(mock(Shortener)).
      storageProvider(null).
      build
  }

  @Test(expected = NullPointerException)
  def void startFailsOnNullStorage() {
    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('/redirect/').
      shortener(mock(Shortener)).
      storageProvider({
        val mockStorageProvider = mock(StorageProvider)
        when(mockStorageProvider.openStorage).thenReturn(null)
        mockStorageProvider
      }).
      build.
    start
  }

  @Test
  def void closesCloseableComponents() {
    val closeable = mock(Closeable)
    UrlShortener.tryAndClose(closeable)
    verify(closeable, times(1)).close()
  }

  @Test
  def void ignoresClosingException() {
    val closeable = mock(Closeable)
    doThrow(new IOException('Oops!')).when(closeable).close
    UrlShortener.tryAndClose(closeable)
    verify(closeable, times(1)).close
  }
}