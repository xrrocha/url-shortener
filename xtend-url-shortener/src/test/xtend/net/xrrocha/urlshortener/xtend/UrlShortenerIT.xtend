package net.xrrocha.urlshortener.xtend

import java.io.ByteArrayOutputStream
import java.io.File
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultRedirectStrategy
import org.apache.http.impl.client.HttpClientBuilder
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import net.xrrocha.urlshortener.xtend.Main
import net.xrrocha.urlshortener.xtend.storage.StorageProvider

import static junit.framework.TestCase.assertEquals
import static org.apache.http.HttpStatus.*
import static spark.Spark.awaitInitialization
import static spark.Spark.get

/**
 * Integration test for Main. Unit tests in this suite are run in sequence
 * and may fail if run individually.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UrlShortenerIT {
  static val BASE_URL = 'http://localhost:4567'
  static val API_URL = BASE_URL + '/api/shorten'

  static val TEST_PATH =
    '/test/ridiculously/long/ugly/path/longUglyFilename.looong'
  static val TEST_URL = BASE_URL + TEST_PATH

  static val REDIRECT_URL = BASE_URL + '/'
  static val String TEST_SHORT_PATH = '770ae8b1'
  static val String EXPECTED_SHORT_URL = REDIRECT_URL + TEST_SHORT_PATH
  static val String EXPECTED_TEST_RESPONSE =
    'Content coming from ridiculously long URL. Not so long itself, though'

  static val dataFile = new File('target/url-shortener.dat')

  static val httpClient = HttpClientBuilder.create.
    setRedirectStrategy(new DefaultRedirectStrategy).
    build

  static val is =
    Main.openConfigFile(Main.DEFAULT_CONFIG_NAME);
  static val UrlShortener urlShortener =
    Main.buildUrlShortener(is, Main.DEFAULT_CONFIG_NAME)

  @BeforeClass
  static def void startup() {
    dataFile.delete

    urlShortener.start
    get('/test/*', [eq, res | EXPECTED_TEST_RESPONSE])

    awaitInitialization
  }

  @AfterClass
  static def shutdown() {
    urlShortener.stop
  }

  @Test
  def s1_shortensValidLongUrl() {
    val response = API_URL.doPost(TEST_URL)

    assertEquals(SC_CREATED, response.statusLine.statusCode)

    val actualShortUrl = response.entity.body
    assertEquals(EXPECTED_SHORT_URL, actualShortUrl)
  }

  @Test
  def s2_failsOnInvalidLongUrl() {
    val invalidLongUrl = 'invalidUrl'
    val response = API_URL.doPost(invalidLongUrl)
    assertEquals(SC_BAD_REQUEST, response.statusLine.statusCode)
  }

  @Test
  def s3_redirectsOnValidShortUrl() {
    val response = EXPECTED_SHORT_URL.doGet
    assertEquals(SC_OK, response.statusLine.statusCode)
    assertEquals(EXPECTED_TEST_RESPONSE, response.entity.body)
  }
  @Test
  def s4_deletesExistingShortUrl() {
    val response = EXPECTED_SHORT_URL.doDelete
    assertEquals(SC_NO_CONTENT, response.statusLine.statusCode)
  }

  @Test
  def s5_failsDeletionOnNonExistingShortUrl() {
    val response = EXPECTED_SHORT_URL.doDelete
    assertEquals(SC_NOT_FOUND, response.statusLine.statusCode)
  }

  static def HttpResponse doPost(String apiUrl, String longUrl) {
    val request = new HttpPost(apiUrl)
    val httpEntity = new StringEntity(longUrl)
    request.entity = httpEntity
    httpClient.execute(request)
  }

  static def HttpResponse doGet(String shortUrl) {
    val request = new HttpGet(shortUrl)
    httpClient.execute(request)
  }

  static def HttpResponse doDelete(String shortUrl) {
    val request = new HttpDelete(shortUrl)
    httpClient.execute(request)
  }

  static def getBody(HttpEntity entity) {
    val baos = new ByteArrayOutputStream
    entity.writeTo(baos)
    baos.toString
  }
}