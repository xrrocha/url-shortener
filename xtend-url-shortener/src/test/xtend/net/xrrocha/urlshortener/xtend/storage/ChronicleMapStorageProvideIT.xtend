package net.xrrocha.urlshortener.xtend.storage

import java.io.File
import org.junit.Test

import static junit.framework.TestCase.assertFalse
import static net.xrrocha.util.Using.using
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

/**
 * ChronicleMap storage provider unit test.
 */
class ChronicleMapStorageProviderIT {
  @Test
  def void persistsDataAcrossClosings() {
    val file = File.createTempFile('data', '.tmp') => [
      deleteOnExit
    ]

    val provider = ChronicleMapStorageProvider.builder.
      name('test').
      filename(file.getAbsolutePath).
      entries(16).
      averageKeySize(8).
      averageValueSize(32).
      build

    using(provider.openStorage) [
      assertTrue(isEmpty)
      assertFalse(containsKey('short'))
      put('short', 'long')
      assertEquals('long', get('short'))
    ]

    using(provider.openStorage) [
      assertEquals('long', get('short'))
    ]
  }

  @Test(expected = NullPointerException)
  def void failsOnNullKey() {
    val file = File.createTempFile('data', '.tmp') => [
      deleteOnExit
    ]

    val provider = ChronicleMapStorageProvider.builder.
      name('test').
      filename(file.getAbsolutePath).
      entries(16).
      averageKeySize(8).
      averageValueSize(32).
      build

    using(provider.openStorage) [
      put(null, 'value')
    ]
  }

  @Test(expected = NullPointerException)
  def void failsOnNullValue() {
    val file = File.createTempFile('data', '.tmp') => [
      deleteOnExit
    ]

    val provider = ChronicleMapStorageProvider.builder.
      name('test').
      filename(file.getAbsolutePath).
      entries(16).
      averageKeySize(8).
      averageValueSize(32).
      build

    using(provider.openStorage) [
      put('key', null)
    ]
  }
}
