package net.xrrocha.urlshortener.xtend.shortening

import org.junit.Test

import static junit.framework.TestCase.assertEquals

/**
 * Murmur3 shortener unit tests.
 */
class Murmur3Test {
  @Test(expected = NullPointerException)
  def void failsOnNullString() {
    new Murmur3Shortener => [
      shorten(null)
    ]
  }

  @Test
  def void shortensProperly() {
    val shortenedString =
      new Murmur3Shortener().shorten('ratherLongValueInNeedOfShortening')
    assertEquals('c8293269', shortenedString)
  }
}