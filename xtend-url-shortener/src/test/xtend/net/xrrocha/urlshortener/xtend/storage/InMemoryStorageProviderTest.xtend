package net.xrrocha.urlshortener.xtend.storage

import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

/**
 * InMemoryStorage unit tests.
 */
class InMemoryStorageProviderTest {
  @Test
  def returnsProperMap() {
    val provider = new InMemoryStorageProvider
    val storage = provider.openStorage
    assertTrue(storage.isEmpty)
    storage.put('short', 'long')
    assertEquals('long', storage.get('short'))
  }
}