package net.xrrocha.urlshortener.xtend

import java.io.Closeable
import java.util.Map
import net.xrrocha.urlshortener.xtend.shortening.Shortener
import net.xrrocha.urlshortener.xtend.storage.StorageProvider
import org.junit.Test

import static org.mockito.Mockito.*

/**
 * Integration test for closing.
 */
class UrlShortenerClosingIT {
  @Test
  def void closesComponentsOnStop() {
    val closeableMap =
      mock(Map,  withSettings().extraInterfaces(Closeable))

    val storageProviderMock = mock(StorageProvider)
    when(storageProviderMock.openStorage).thenReturn(closeableMap)

    UrlShortener.builder.
      apiPath('/api/shorten').
      redirectPath('/').
      shortener(mock(Shortener)).
      storageProvider(storageProviderMock).
      build => [
        start
        stop
      ]

    verify(closeableMap as Closeable, times(1)).close()
  }
}