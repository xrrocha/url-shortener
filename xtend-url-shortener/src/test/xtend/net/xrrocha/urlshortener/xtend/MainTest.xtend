package net.xrrocha.urlshortener.xtend

import java.security.Permission
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

import static java.lang.System.*
import static junit.framework.TestCase.assertEquals

/**
 * Unit tests for Main.
 */
public class MainTest {
  /**
   * A custom <code>SecurityManager</code> is instated that traps and ignores
   * invocations to <code>System.exit(1)</code>. The system security manager
   * is reinstated after running all tests.
   */
  private static val systemSecurityManager = System.securityManager

  private static final SecurityManager mainSecurityManager =
  new SecurityManager() {
    override checkPermission(Permission permission) {
      if('exitVM.1'.equals(permission.name)) {
        throw new SecurityException('System.exit() blocked.')
      }
    }
  }

  @BeforeClass
  static def setSecurityManager() {
    System.securityManager = mainSecurityManager
  }

  @AfterClass
  static def restoreSecurityManager() {
    System.securityManager = systemSecurityManager
  }

  @Test
  def usesDefaultFilenameOnNoArgs() {
    val filename = Main.configFromArgs
    assertEquals(Main.DEFAULT_CONFIG_NAME, filename)
  }

  @Test
  def returnsFilenameWithOneArg() {
    val expectedFilename = 'expected.yaml'
    val actualFilename = Main.configFromArgs(expectedFilename)
    assertEquals(expectedFilename, actualFilename)
  }

  @Test(expected = SecurityException)
  def void exitsWhenMoreThanOneArg() {
    Main.configFromArgs('one.yaml', 'two.yaml')
  }

  @Test(expected = SecurityException)
  def void exitsOnNonExistentResource() {
    Main.openConfigFile('non-existent.yaml')
  }

  @Test(expected = SecurityException)
  def void exitsOnEmptyYaml() {
    val is = Main.openConfigFile('empty.yaml')
    Main.buildUrlShortener(is, 'empty.yaml')
  }

  @Test(expected = SecurityException)
  def void exitsOnBadYaml() {
    val is = Main.openConfigFile('bad.yaml')
    Main.buildUrlShortener(is, 'bad.yaml')
  }

  @Test
  def returnsDefaultConfigFilenameOnNoArg() {
    assertEquals(Main.DEFAULT_CONFIG_NAME, Main.configFromArgs)
  }

  @Test
  def returnsConfigFilenameOnOneArg() {
    val expectedConfigFilename = 'explicit-filename.yaml'
    assertEquals(expectedConfigFilename,
    Main.configFromArgs(expectedConfigFilename))
  }

  @Test(expected = SecurityException)
  def void exitsOnMoreThanOneArg() {
    Main.configFromArgs('param1', 'param2')
  }

}
