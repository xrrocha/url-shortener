package net.xrrocha.urlshortener.xtend.storage

import org.junit.Test

/**
 * Unit tests for ChronicleMap storage provider.
 */
class ChronicleMapStorageProviderTest {

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnNullName() {
    ChronicleMapStorageProvider.builder.
      name(null).
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnEmptyName() {
    ChronicleMapStorageProvider.builder.
      name('').
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnBlankName() {
    ChronicleMapStorageProvider.builder.
      name(' \t\n').
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnNullFilename() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename(null).
      entries(1048576).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnEmptyFilename() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnBlankFilename() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename(' \t\n').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnZeroEntries() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('/tmp/keyValue.dat').
      entries(0).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnNegativeEntries() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('/tmp/keyValue.dat').
      entries(-1).
      averageKeySize(32).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnZeroAverageKeySize() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(0).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnNegativeAverageKeySize() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(-1).
      averageValueSize(64).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnZeroAverageValueSize() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(0).
      build
  }

  @Test(expected = IllegalArgumentException)
  def void failsValidationOnNegativeAverageValueSize() {
    ChronicleMapStorageProvider.builder.
      name('someName').
      filename('/tmp/keyValue.dat').
      entries(1048576).
      averageKeySize(32).
      averageValueSize(-1).
      build
  }
}