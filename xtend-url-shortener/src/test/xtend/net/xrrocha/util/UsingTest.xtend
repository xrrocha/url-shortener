package net.xrrocha.util

import java.io.Closeable
import org.junit.Test

import static net.xrrocha.util.Using.using
import static org.junit.Assert.*
import static org.mockito.Mockito.*

/**
 * Unit tests for <code>Using</code>.
 */
class UsingTest {
  @Test
  def void ignoresNonCloseable() {
    assertEquals(
            'someString'.length,
            <String, Integer>using('someString') [ s | s.length ])
  }

  @Test
  def void closesResourceOnFunctionSuccess() {
    val closeable = mock(Closeable)
    using(closeable) [ toString ]
    verify(closeable).close
  }

  @Test(expected = UnsupportedOperationException)
  def void closesResourceOnFunctionFailure() {
    val closeable = mock(Closeable)
    using(closeable) [ throw new UnsupportedOperationException('Oops!') ]
    verify(closeable).close
  }

  @Test(expected = UnsupportedOperationException)
  def void ignoresExceptionOnFunctionFailure() {
    val closeable = mock(Closeable)
    doThrow(new IllegalStateException('Shoot!')).when(closeable).close
    using(closeable) [ throw new UnsupportedOperationException('Oops!') ]
    verify(closeable).close
  }

  @Test
  def void closesResourceOnProcedureSuccess() {
    val closeable = mock(Closeable);
    <Closeable>using(closeable) [ println ]
    verify(closeable).close
  }

  @Test(expected = UnsupportedOperationException)
  def void closesResourceOnProcedureFailure() {
    val closeable = mock(Closeable);
    <Closeable>using(closeable) [
      throw new UnsupportedOperationException('Oops!')
    ]
    verify(closeable).close
  }

  @Test(expected = UnsupportedOperationException)
  def void ignoresExceptionOnProcedureFailure() {
    val closeable = mock(Closeable)
    doThrow(new IllegalStateException('Shoot!')).when(closeable).close;
    <Closeable>using(closeable) [
      throw new UnsupportedOperationException('Oops!')
    ]
    verify(closeable).close
  }
}