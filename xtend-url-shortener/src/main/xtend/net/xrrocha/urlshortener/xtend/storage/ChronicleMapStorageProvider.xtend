package net.xrrocha.urlshortener.xtend.storage

import de.oehme.xtend.contrib.Buildable
import java.io.File
import net.openhft.chronicle.map.ChronicleMapBuilder

import static com.google.common.base.Preconditions.checkArgument
import org.eclipse.xtend.lib.annotations.Data

/**
 * <p>ChronicleMap-based storage provider implementation. This class persists
 * key/value pairs on the filesystem. ChronicleMap files require to be closed
 * upon deactivation. Currently, no attempt is made to recover the persistent
 * file in case of abrupt shutdown.</p>
 */
@Buildable
class ChronicleMapStorageProvider implements StorageProvider {
  /**
   * A free-form name globaly unique across all active ChronicleMaps on the
   * same JVM.
   */
  val String name
  /**
   * The name of the file to be used as persitent store. Will be created if
   * not yet existent. Existent files may be corrupted if the associated map
   * is not properly
   * closed.
   */
  val String filename
  /**
   * The effective maximum number of key/value pairs to be held in the map.
   */
  val int entries
  /**
   * The average key size in bytes.
   */
  val double averageKeySize
  /**
   * The average value size in bytes.
   */
  val double averageValueSize

  private new() {
    name = null
    filename = null
    entries = 0
    averageKeySize = 0d
    averageValueSize = 0d
  }

  new(String name,
      String filename,
      int entries,
      double averageKeySize,
      double averageValueSize) {
    this.name = name
    this.filename = filename
    this.entries = entries
    this.averageKeySize = averageKeySize
    this.averageValueSize = averageValueSize

    validate
  }

  /**
   * Build a new ChronicleMap instance with the given configuration.
   * @return the newly created <code>Map&ltString, String&gt</code> backed
   * by a file store.
   */
  override openStorage() {
    ChronicleMapBuilder.
      of(String, String).
      name(this.name).
      entries(this.entries).
      averageKeySize(this.averageKeySize).
      averageValueSize(this.averageValueSize).
      createPersistedTo(new File(filename))
  }

  def validate() {
    checkArgument(name !== null && name.trim.length > 0,
                  'Name cannot be null or blank')
    checkArgument(filename !== null && filename.trim.length > 0,
                  'Name cannot be null or blank')
    checkArgument(entries  > 0d,
                  'Entries cannot be zero or negative')
    checkArgument(averageKeySize  > 0d,
                  'Average key size cannot be zero or negative')
    checkArgument(averageValueSize  > 0d,
                  'Average value size cannot be zero or negative')
  }
}