package net.xrrocha.urlshortener.xtend

import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import org.slf4j.LoggerFactory
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.introspector.BeanAccess

/**
 * <p>Application to build and activate a RESTful <code>UrlShortener</code>
 * instance.</p>
 * <p>A ready-made <code>UrlShortener</code> is created from a Yaml resource
 * file. This fileis expect to contain the full instantiation of a URL
 * shortener together with all its dependencies.</p>
 */
class Main {
  /**
   * The default resource Yaml file to use if not given as a command-line
   * argument.
   */
  package static val DEFAULT_CONFIG_NAME = 'url-shortener.yaml'

  private static val logger = LoggerFactory.getLogger(Main)

  static def main(String... args) {
    val configFilename = configFromArgs(args)
    logger.debug('Configuration filename: {}', configFilename)

    val in = openConfigFile(configFilename)

    val urlShortener = buildUrlShortener(in, configFilename)

    startUrlShortener(urlShortener)
  }

  /**
   * Start a newly created instance of <code>UrlShortener</code>. Failure to
   * start results in application exit. Adds hook to close the
   * <code>UrlShortener</code> gracefully upon JVM shutdown.
   * @param urlShortener
   */
  static def startUrlShortener(UrlShortener urlShortener) {
    try {
      urlShortener.start
      logger.info('REST server started')
    } catch(Exception e) {
      // Failure to start is associated with incorrect configuration or
      // misbehaving dependencies
      exit('''Error starting URL shortener: «e.message»''')
    }

    // Make sure disposable dependencies are gracefully stopped upon JVM
    // shutdown
    Runtime.runtime.addShutdownHook(new Thread[
      logger.info("Stopping URL shortener service")
      urlShortener.stop
    ])
  }

  /**
   * Build a new instance of <code>UrlShortener</code> from a Yaml
   * configuration resource file.
   *
   * @param resourceName The name of the Yaml resource to be loaded
   * @param filename The filename to use for error messages
   * @return The ready-made <code>UrlShortener</code> instance entirely
   * created by SnakeYaml.
   */
  static def buildUrlShortener(InputStream in, String filename) {
    val yaml = new Yaml => [
      beanAccess = BeanAccess.FIELD
    ]
    val urlShortener =
      try {
        yaml.loadAs(in, UrlShortener)
      } catch(Exception e) {
        exit('''Error in configuration file '«filename»': «e»''')
        throw new IllegalStateException('unreachable')
      }

    if(urlShortener !== null) {
      urlShortener
    } else {
      exit('''Invalid yaml content in configuration file "«filename»"''')
    }
  }

  /**
   * Opens the configuration file looking for it on the file systema and,
   * failing that, as a classpath resource.
   *
   * @param filename The configuration filename
   * @return The input stream associated with the file
   */
  static def openConfigFile(String filename) {
    val is =
      if(new File(filename).exists()) {
        try {
          new FileInputStream(filename)
        } catch(FileNotFoundException e) {
          exit('''Error opening configuration file "«filename»": «e»''')
          // unreachable, but needed
          null
        }
      } else {
        val classLoader = Thread.currentThread().contextClassLoader
        classLoader.getResourceAsStream(filename)
      }

    if(is === null) {
      exit('''Cannot open configuration file: «filename»''')
    }

    return is
  }

  /**
   * Extract the configuration resource file name from the command line
   * arguments.
   *
   * @param args The arguments passed to <code>main</code> from the command line
   * @return The configuration resource filename.
   */
  static def configFromArgs(String... args) {
    switch(args.length) {
      case 0:
        DEFAULT_CONFIG_NAME
      case 1:
        args.get(0)
      default:
        exit('''Usage: «Main.name» <yamlFile|«DEFAULT_CONFIG_NAME»>''')
    }
  }

  static def exit(String message) {
    System.err.println(message)
    System.exit(1)
  }

}