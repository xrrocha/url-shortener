package net.xrrocha.urlshortener.xtend.shortening

/**
 * String shortener.
 */
interface Shortener {
  /**
   * Shorten a string by applying an appropriate hash function so as to
   * minimize the possibility of collisions.
   * @param string The string to be shortened
   * @return The shortened string
   */
  def String shorten(String string)
}