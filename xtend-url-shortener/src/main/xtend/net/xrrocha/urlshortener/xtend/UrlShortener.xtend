package net.xrrocha.urlshortener.xtend

import de.oehme.xtend.contrib.Buildable
import java.io.Closeable
import java.net.URI
import java.util.Map
import net.xrrocha.urlshortener.xtend.shortening.Shortener
import net.xrrocha.urlshortener.xtend.storage.StorageProvider
import org.slf4j.LoggerFactory
import spark.Request
import spark.Response
import spark.Spark

import static com.google.common.base.Preconditions.checkArgument
import static com.google.common.base.Preconditions.checkNotNull
import static org.apache.http.HttpStatus.*
import static spark.Spark.*

/**
 * <p>HTTP service exposing URL shortening operations through a REST API.
 * This class is the workhorse of the URL shoertening service.</p>
 * <p>The API URL can be configured through setter and defaults to '/api/shorten'</p>
 * <p>This class has dependencies on two interface types:</p>
 * <ul>
 * <li><code>Shortener</code>: the component in charge of computing a short
 * hash from a given URL.</li>
 * <li><code>StorageProvider</code>: the <code>Map&ltString, String&gt
 * </code> storage where shortUrl/longUrl key/value pairs are stored. This
 * map will typically be persisted.</li>
 * </ul>
 */
@Buildable
class UrlShortener {
  /**
   * The component in charge of deriving a short hash from a given URL.
   */
  val Shortener shortener
  /**
   * The component in charge of providing a key/value store exposed as a
   * <code>Map&ltString, String&gt</code>.
   */
  val StorageProvider storageProvider
  /**
   * The API path prefix to be used for REST endpoints.
   */
  val String apiPath
  /**
   * The redirect path prefix to be used for retrieving short URL's.
   */
  val String redirectPath

  /**
   * The <code>Map&ltString, String&gt</code> used to store
   * shortUrl/longUrl key/value pairs. This map is drawn once -at startup-
   * from the <code>StorageProvider</code> dependency.
   */
  var Map<String, String> storage
  /**
   * The logger instance
   */
  static val logger = LoggerFactory.getLogger(UrlShortener)

  private new() {
    shortener = null
    storageProvider = null
    apiPath = null
    redirectPath = null
  }

  new(Shortener shortener,
      StorageProvider storageProvider,
      String apiPath,
      String redirectPath)
  {
    this.shortener = shortener
    this.storageProvider = storageProvider
    this.apiPath = apiPath
    this.redirectPath = redirectPath

    validate
  }

  /**
   * <p>Implicitly start a <em>default</em> <code>Spark</code> service
   * instance by defining all the available REST endpoints:</p>
   * <ul>
   * <li><code>POST /api/shorten</code>: Shorten URL passed as POST body</li>
   * <li><code>GET /api/shorten/:shortUrl</code>: Redirect to long URL based on the
   * short URL requested as GET</li>
   * <li><code>DELETE /api/shorten/:shortUrl</code>: Remove short/long URL pair given
   * by :shortUrl</li>
   * </ul>
   * <p>The content type is <code>text/plain</code> for all endpoints</p>
   */
  def start() {
    validate

    storage = storageProvider.openStorage
    if(storage === null) {
      val errorMessage = 'Storage provider yielded null storage'
      logger.error(errorMessage)
      throw new NullPointerException(errorMessage)
    }
    /*
     * Shorten and store a long url passed as POST body. The returned entity
     * body contains the shortened URL to be used in lieu of the input long one.
     */
    post(apiPath, [req, res |
      val longUrl = req.body
      try {
        val uri = new URI(longUrl)
        if(uri.scheme !== null && uri.scheme.startsWith('http')) {
          val hash = longUrl.shorten
          storage += hash -> longUrl
          val shortUrl = req.shortUrlFrom(hash)
          res.respondWith(SC_CREATED, shortUrl)
        } else {
          res.respondWith(SC_BAD_REQUEST, '''Not an HTTP URL: «longUrl»''')
        }
      } catch(Exception e) {
        res.respondWith(SC_BAD_REQUEST, '''Malformed long URL: «longUrl»''')
      }
    ])

    /*
     * Expand short URL (pointed to by :hash) and redirect to it
     */
    get('''«redirectPath»:hash''', [req, res |
      val hash = req.params(':hash')
      if(storage.containsKey(hash)) {
        val longUrl = storage.get(hash)
        res.redirect(longUrl)
        ''
      } else {
        res.respondWith(SC_NOT_FOUND, '''No such short URL: «req.url»''')
      }
    ])

    /*
     * Delete short/long URL pair given by :hash
     */
    delete('''«redirectPath»:hash''', [req, res |
      val hash = req.params(':hash')
      if(storage.containsKey(hash)) {
        storage -= hash
        res.respondWith(SC_NO_CONTENT)
      } else {
        res.respondWith(SC_NOT_FOUND)
      }
    ])
  }

  /**
   * Stop the Spark default service and close the shortener and/or storage if
   * they implement <code>Closeable</code>.
   */
  def stop() {
    Spark.stop()
    tryAndClose(storage)
  }

  /**
   * Try and close and object if it implements <code>Closeable</code>
   * @param obj The object to be closed if needed
   */
  static def tryAndClose(Object obj) {
    if (obj instanceof Closeable) {
      try {
        obj.close
      } catch(Exception e) {
        logger.warn('Ignoring error closing component: {}', e as Object)
      }
    }
  }

  /**
   * Validate configuration and dependencies prior to starting Spark default
   * service.
   */
  def void validate() {
    checkArgument(
            apiPath !== null && apiPath.trim.length > 0,
            'API path cannot be null or blank')
    checkArgument(
            apiPath.startsWith('/'),
            'Redirect path must start and end with slash')
    checkArgument(
            redirectPath !== null && redirectPath.trim.length > 0,
            'Redirect path cannot be null or blank')
    checkArgument(
            redirectPath.startsWith('/') && redirectPath.endsWith('/'),
            'Redirect path must start and end with slash')
    checkNotNull(shortener, 'Shortener cannot be null')
    checkNotNull(storageProvider, 'Storage provider cannot be null')
  }

  /**
   * Shorten a long URL by passing it to the <code>shortener.shorten()</code>
   * method.
   * @param longUrl the long URL to be shortened
   * @return the shortened URL
   */
  def shorten(String longUrl) {
    shortener.shorten(longUrl)
  }

  /**
   * Put a new hash/longUrl pair into the storage map.
   * @param storage the storage map
   * @param pair the hash/longUrl pair
   */
  def operator_add(Map<String, String> storage, Pair<String, String> pair) {
    storage.put(pair.key, pair.value)
  }

  /**
   * Remove an en existing hash/longUrl pair from the storage map.
   * @param storage the storage map
   * @param hash the hash pointing to a longUrl
   */
  def operator_remove(Map<String, String> storage, String hash) {
    storage.remove(hash)
  }


  /**
   * Create a redirect URL by resolving the <code>redirectPath</code> against
   * the <code>request</code> URL.
   * @param request the Spark request
   * @param hash the hash from which to build a redirect URL
   * @return the redirect URL associated with the short path
   */
  def shortUrlFrom(Request request, String hash) {
    new URI(request.url).resolve(redirectPath + hash).toString
  }

  /**
   * Complete response returning a string body
   * @param response The response to be completed
   * @param status The Http status
   * @return An empty request body
   */
  def respondWith(Response response, int status) {
    respondWith(response, status, "")
  }

  /**
   * Complete response returning a string body
   * @param response The response to be completed
   * @param status The Http status
   * @return The request body string
   */
  def respondWith(Response response, int status, String body) {
    response.status(status)
    body
  }

}