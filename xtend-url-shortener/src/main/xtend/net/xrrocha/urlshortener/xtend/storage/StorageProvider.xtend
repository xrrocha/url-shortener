package net.xrrocha.urlshortener.xtend.storage

import java.util.Map
import java.util.concurrent.ConcurrentHashMap

/**
 * Storage provider. <em>Storage</em> implements the <code>java.util
 * .Map</code> interface to embody the notion of a (potentially persistent)
 * key/value store.
 */
interface StorageProvider {
  /**
   * Return a newly created <code>Map&ltString, String&gt</code> for
   * clients to use as a
   * key/value store. Stores returned can be persistent in which case they
   * will typically
   * implement <code>Closeable</code> as well clients should make an attempt
   * to explicitly close
   * the returned map in this case. Implementations are expected to return a
   * brand new map upon
   * each invocation.
   * @return The newly create map to act as a (potentially persistent)
   * key/value storage.
   */
  def Map<String, String> openStorage()
}


/**
 * A simple, in-memory storage provider returning a concurrent hash map.
 */
class InMemoryStorageProvider implements StorageProvider {
  /**
   * Return a new concurrent hash map.
   * @return the newly created in-memory map.
   */
  override openStorage() {
    new ConcurrentHashMap<String, String>
  }
}
