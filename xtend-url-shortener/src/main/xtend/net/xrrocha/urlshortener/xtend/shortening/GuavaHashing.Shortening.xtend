package net.xrrocha.urlshortener.xtend.shortening

import com.google.common.hash.Hashing
import java.nio.charset.StandardCharsets

import static com.google.common.base.Preconditions.checkNotNull

/**
 * Murmur3-based shortener. This class only supports UTF-8.
 */
class Murmur3Shortener implements Shortener {
  override shorten(String string) {
    checkNotNull(string, 'String to be shortened cannot be null')
    Hashing.murmur3_32().
      hashString(string, StandardCharsets.UTF_8).
      toString()
  }
}

/**
 * SipHash-based shortener. This class only supports UTF-8.
 */
class SipHashShortener implements Shortener {
  override shorten(String string) {
    checkNotNull(string, 'String to be shortened cannot be null')
    Hashing.sipHash24().
      hashString(string, StandardCharsets.UTF_8).
      toString
  }
}
