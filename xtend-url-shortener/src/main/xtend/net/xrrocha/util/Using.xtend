package net.xrrocha.util

import java.io.Closeable
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1

/**
 * Implement Java's try-with-resources.
 */
class Using {
  def static <T> Object using(T resource, Procedure1<T> lambda) {
    using(resource, [
      lambda.apply(resource)
      null
    ])
  }

  def static <T, R> R using(T resource, (T) => R lambda) {
    var Throwable throwable = null

    try {
      lambda.apply(resource)
    } catch(Throwable t) {
      throwable = t
      throw t
    } finally {
      if (resource instanceof Closeable) {
        if(throwable === null) {
          resource.close
        } else {
          try {
            resource.close
          } catch(Throwable unused) {
          }
        }
      }
    }
  }
}