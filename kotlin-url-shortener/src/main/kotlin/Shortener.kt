package net.xrrocha.urlshortener.kotlin.shortening


import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import com.google.common.hash.Hashing
import java.nio.charset.StandardCharsets

/**
 * String shortening.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes(
        Type(value = Murmur3Shortener::class, name = "murmur3"),
        Type(value = SipHashShortener::class, name = "sipHash")
)
interface Shortener {
    /**
     * Shorten a string by applying an appropriate hash function so as to minimize the
     * possibility of collisions.
     *
     * @param string The string to be shortened
     * @return The shortened string
     */
    fun shorten(string: String): String
}

/**
 * Murmur3-based shortening. This class only supports UTF-8.
 */
@JsonTypeName("murmur3")
class Murmur3Shortener : Shortener {
    override fun shorten(string: String) =
            Hashing.murmur3_32().
                    hashString(string, StandardCharsets.UTF_8).
                    toString()
}

/**
 * SipHash-based shortening. This class only supports UTF-8.
 */
@JsonTypeName("sipHash")
class SipHashShortener : Shortener {
    override fun shorten(string: String) =
            Hashing.sipHash24().
                    hashString(string, StandardCharsets.UTF_8).
                    toString()
}
