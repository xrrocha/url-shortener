package net.xrrocha.urlshortener.kotlin

import net.xrrocha.urlshortener.kotlin.shortening.Shortener
import net.xrrocha.urlshortener.kotlin.storage.StorageProvider
import org.apache.http.HttpStatus.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.Request
import spark.Response
import spark.Spark
import spark.Spark.*
import java.io.Closeable
import java.net.URI


/**
 * HTTP service exposing URL shortening operations through a REST API.
 * This class is the workhorse of the URL shoertening service.
 *
 * The API URL can be configured through setter and defaults to "/api/shorten"
 * This class has dependencies on two interface types:
 *
 * - `Shortener`: the component in charge of computing a short hash from a given URL
 * - `StorageProvider`: the `Map<String, String>` storage where shortUrl/longUrl key/value pairs
 * are stored. This map will typically be persisted
 */
open class UrlShortener(
        /**
         * The component in charge of deriving a short hash from a given URL.
         */
        private val shortener: Shortener,
        /**
         * The component in charge of providing a key/value store exposed as a
         * `Map<String, String>`.
         */
        private val storageProvider: StorageProvider,
        /**
         * The API path prefix to be used for REST endpoints.
         */
        private val apiPath: String,
        /**
         * The redirect path prefix to be used for retrieving short URL"s.
         */
        private val redirectPath: String
) {

    companion object {
        private val logger: Logger by lazy { LoggerFactory.getLogger(this::class.java.name) }
    }

    private var storage: MutableMap<String, String>? = null

    init {
        require(apiPath.isNotEmpty()) {
            "API path cannot be blank"
        }
        require(apiPath.startsWith("/") && apiPath.endsWith("/")) {
            "API path must end with '/'"
        }
        require(redirectPath.isNotEmpty()) {
            "Redirect path cannot be blank"
        }
        require(redirectPath.startsWith("/") && redirectPath.endsWith("/")) {
            "Redirect path must end with '/'"
        }
    }

    fun start() {
        logger.debug("Opening storage")
        val localStorage = storageProvider.openStorage()
        storage = localStorage

        // Shorten and store a long url passed as POST body. The returned entity body contains the
        // shortened URL to be used in lieu of the input long one
        post(apiPath) { req, res ->
            logger.debug("Processing POST {}", apiPath)
            val longUrl = req.body()
            try {
                val uri = URI(longUrl)
                if (uri.scheme.startsWith("http")) {
                    val hash = longUrl.shorten()
                    localStorage += hash to longUrl
                    val shortUrl = req.shortUrlFrom(hash)
                    res.respondWith(SC_CREATED, shortUrl)
                } else {
                    res.respondWith(SC_BAD_REQUEST, "Not an HTTP URL: $longUrl")
                }
            } catch (e: Exception) {
                res.respondWith(SC_BAD_REQUEST, "Malformed long URL: $longUrl")
            }
        }

        // Expand short URL (pointed to by :hash) and redirect to it
        get("$redirectPath:hash") { req, res ->
            val hash = req.params(":hash")
            if (localStorage.containsKey(hash)) {
                val longUrl = localStorage[hash]
                res.redirect(longUrl)
                ""
            } else {
                res.respondWith(SC_NOT_FOUND, "No such short URL: ${req.url()}")
            }
        }

        // Delete short/long URL pair given by :hash
        delete("$redirectPath:hash") { req, res ->
            val hash = req.params(":hash")
            if (localStorage.containsKey(hash)) {
                localStorage -= hash
                res.respondWith(SC_NO_CONTENT)
            } else {
                res.respondWith(SC_NOT_FOUND)
            }
        }

    }

    fun stop() {
        Spark.stop()
        storage?.let {
            if (it is Closeable) {
                it.close()
            }
        }
    }

    /**
     * Shorten a long URL by passing it to the `shortening.shorten()` method.
     * @return the shortened URL
     */
    fun String.shorten() = shortener.shorten(this)

    /**
     * Create a redirect URL by resolving the `redirectPath` against the `request` URL.
     * @param hash the hash from which to build a redirect URL
     * @return the redirect URL associated with the short path
     */
    fun Request.shortUrlFrom(hash: String): String =
            URI(this.url()).resolve(redirectPath + hash).toString()

    /**
     * Complete response returning a string body
     * @param status The Http status
     * @return An empty request body
     */
    fun Response.respondWith(status: Int) = respondWith(status, "")

    /**
     * Complete response returning a string body
     * @param status The Http status
     * @param body The response body
     * @return The request body string
     */
    fun Response.respondWith(status: Int, body: String): String {
        this.status(status)
        this.body(body)
        return body
    }
}