package net.xrrocha.urlshortener.kotlin.storage

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import net.openhft.chronicle.map.ChronicleMapBuilder
import java.io.File
import java.util.concurrent.ConcurrentHashMap

/**
 * Storage provider. _Storage_ implements the `java.util.Map` interface to embody the notion of
 * a (potentially persistent) key/value store.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes(
        JsonSubTypes.Type(value = InMemoryStorageProvider::class, name = "inMemory"),
        JsonSubTypes.Type(value = ChronicleMapStorageProvider::class, name = "chronicleMap")
)
interface StorageProvider {
    /**
     * Return a newly created `Map<String, String>` for clients to use as a key/value store.
     * Stores returned can be persistent in which case they will typically implement `Closeable`
     * as well clients should make an attempt to explicitly close the returned map in this case.
     * Implementations are expected to return a brand new map upon each invocation.
     *
     * @return The newly create map to act as a (potentially persistent) key/value storage.
     */
    fun openStorage(): MutableMap<String, String>
}

/**
 * A simple, in-memory storage provider returning a concurrent hash map.
 */
@JsonTypeName("inMemory")
class InMemoryStorageProvider : StorageProvider {
    /**
     * Return a new concurrent hash map.
     * @return the newly created in-memory map.
     */
    override fun openStorage() = ConcurrentHashMap<String, String>()
}

/**
 * ChronicleMap-based storage provider implementation. This class persists
 * key/value pairs on the filesystem. ChronicleMap files require to be closed
 * upon deactivation. Currently, no attempt is made to recover the persistent
 * file in case of abrupt shutdown.
 */
@JsonTypeName("chronicleMap")
data class ChronicleMapStorageProvider(
        /**
         * A free-form name globaly unique across all active ChronicleMaps on the
         * same JVM.
         */
        private val name: String,
        /**
         * The name of the file to be used as persitent store. Will be created if
         * not yet existent. Existent files may be corrupted if the associated map
         * is not properly
         * closed.
         */
        private val filename: String,
        /**
         * The effective maximum number of key/value pairs to be held in the map.
         */
        private val entries: Long,
        /**
         * The average key size in bytes.
         */
        private val averageKeySize: Double,
        /**
         * The average value size in bytes.
         */
        private val averageValueSize: Double
) : StorageProvider {

    init {
        require(name.trim().isNotEmpty()) { "Name cannot beblank" }
        require(filename.trim().isNotEmpty()) { "File name cannot be blank" }
        require(entries > 0L) { "Entries cannot be zero or negative" }
        require(averageKeySize > 0.0) { "Average key size cannot be zero or negative" }
        require(averageValueSize > 0.0) { "Average value size cannot be zero or negative" }
    }

    /**
     * Build a new ChronicleMap instance with the given configuration.
     * @return the newly created <code>Map&ltString, String&gt</code> backed
     * by a file store.
     */
    override fun openStorage(): MutableMap<String, String> {
        val file = File(filename.trim())
        file.parentFile.mkdirs()

        return ChronicleMapBuilder.
                of(String::class.java, String::class.java).
                name(name.trim()).
                entries(entries).
                averageKeySize(averageKeySize).
                averageValueSize(averageValueSize).
                createPersistedTo(file)
    }

}