package net.xrrocha.urlshortener.kotlin

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream

fun main(args: Array<String>) {
    Main.run(args)
}

/**
 * Build and activate a RESTful `UrlShortener` service.
 */
object Main {

    const val DEFAULT_CONFIG_NAME = "url-shortener.json"

    val logger: Logger by lazy { LoggerFactory.getLogger(this::class.java.name) }

    /**
     * Build and activate a RESTful `UrlShortener` service.
     *
     * A ready-made `UrlShortener` inputStream created from a Yaml resource file. This file inputStream expected to
     * contain the full instantiation of a URL shortening together with all its dependencies.
     */
    fun run(args: Array<String>) {

        val configFilename = configFromArgs(args)
        logger.debug("Configuration filename: {}", configFilename)

        val inputStream = openConfigFile(configFilename)

        val urlShortener = buildUrlShortener(inputStream, configFilename)

        startUrlShortener(urlShortener)
    }

    /**
     * Start a newly created instance of `UrlShortener`. Failure to start results inputStream application
     * exit. Adds hook to close the`UrlShortener` gracefully upon JVM shutdown.
     * @param urlShortener The `UrlShortener` service instance
     */
    fun startUrlShortener(urlShortener: UrlShortener) {
        try {
            urlShortener.start()
            logger.info("REST server started")
        } catch (e: Exception) {
            // Failure to start inputStream associated with incorrect configuration or
            // misbehaving dependencies
            e.printStackTrace()
            exit<Unit>("Error starting URL shortening: ${e.message}")
        }

        // Make sure disposable dependencies are gracefully stopped upon JVM shutdown
        Runtime.getRuntime().addShutdownHook(Thread {
            logger.info("Stopping URL shortening service")
            urlShortener.stop()
        })
    }

    /**
     * Build a new instance of `UrlShortener` from a Yaml onfiguration resource file.
     *
     * @param resourceName The name of the Yaml resource to be loaded
     * @param filename The filename to use for error messages
     * @return The ready-made `UrlShortener` instance created by SnakeYaml.
     */
    fun buildUrlShortener(inputStream: InputStream, filename: String): UrlShortener {

        val yamlFactory = YAMLFactory()
        val mapper = ObjectMapper(yamlFactory)
        val kotlinModule = KotlinModule()
        mapper.registerModule(kotlinModule)

        return inputStream.use {
            try {
                mapper.readValue(it, UrlShortener::class.java)
            } catch (e: Exception) {
                exit<UrlShortener>("Error in configuration file '$filename': $e")
            }
        }
    }

    /**
     * Opens the configuration file looking for it on the file systema and, failing that, as a
     * classpath resource.
     *
     * @param filename The configuration filename
     * @return The inputStream stream associated with the file
     */
    fun openConfigFile(filename: String): InputStream {
        val inputStream =
                if (File(filename).exists()) {
                    try {
                        FileInputStream(filename)
                    } catch (e: FileNotFoundException) {
                        exit<InputStream>("Error opening configuration file '$filename': $e")
                    }
                } else {
                    val classLoader = Thread.currentThread().contextClassLoader
                    classLoader.getResourceAsStream(filename)
                }

        return inputStream ?: exit("Cannot open configuration file: ${filename}")
    }

    /**
     * Extract the configuration resource file name from the command line
     * arguments.
     *
     * @param args The arguments passed to `main` from the command line
     * @return The configuration resource filename.
     */
    fun configFromArgs(args: Array<String>): String {
        return when (args.size) {
            0 -> DEFAULT_CONFIG_NAME
            1 -> args[0]
            else -> exit<String>("Usage: ${Main::class} <yamlFile|$DEFAULT_CONFIG_NAME>")
        }
    }

    fun <T> exit(message: String): T {
        System.err.println(message)
        System.exit(1)
        throw IllegalArgumentException("System.exit(1) failed?")
    }
}