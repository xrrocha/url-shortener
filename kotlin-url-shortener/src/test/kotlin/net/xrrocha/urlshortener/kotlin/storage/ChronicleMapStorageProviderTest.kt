package net.xrrocha.urlshortener.kotlin.storage

import org.junit.Test

/**
 * Unit tests for ChronicleMap storage provider.
 */
class ChronicleMapStorageProviderTest {

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnBlankName() {
        ChronicleMapStorageProvider(
                name = "",
                filename = "target/chronicleMap.dat",
                entries = 16L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnEmptyName() {
        ChronicleMapStorageProvider(
                name = "",
                filename = "target/chronicleMap.dat",
                entries = 16L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnEmptyFilename() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "",
                entries = 16L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnBlankFilename() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = " \t\n",
                entries = 16L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnZeroEntries() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "target/chronicleMap.dat",
                entries = 0L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnNegativeEntries() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "target/chronicleMap.dat",
                entries = -1L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnZeroAverageKeySize() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "target/chronicleMap.dat",
                entries = 1048576L,
                averageKeySize = 0.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnNegativeAverageKeySize() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "target/chronicleMap.dat",
                entries = 1048576L,
                averageKeySize = -1.0,
                averageValueSize = 32.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnZeroAverageValueSize() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "target/chronicleMap.dat",
                entries = 1048576L,
                averageKeySize = 8.0,
                averageValueSize = 0.0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun failsValidationOnNegativeAverageValueSize() {
        ChronicleMapStorageProvider(
                name = "chronicleMap",
                filename = "target/chronicleMap.dat",
                entries = 1048576L,
                averageKeySize = 8.0,
                averageValueSize = -1.0)
    }
}
