package net.xrrocha.urlshortener.kotlin.storage

import org.junit.Test

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue

/**
 * InMemoryStorage unit tests.
 */
class InMemoryStorageProviderTest {
    @Test
    fun returnsProperMap() {
        val provider = InMemoryStorageProvider()
        val storage = provider.openStorage()
        assertTrue(storage.isEmpty())
        storage.put("short", "long")
        assertEquals("long", storage["short"])
    }
}
