package net.xrrocha.urlshortener.kotlin

import net.xrrocha.urlshortener.kotlin.shortening.Shortener
import net.xrrocha.urlshortener.kotlin.storage.StorageProvider
import org.junit.Test
import org.mockito.Mockito.mock

/**
 * URL shortening unit tests.
 */
class UrlShortenerTest {

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnEmptyApiPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = "",
                redirectPath = "/")
    }

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnBlankApiPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = " \t\n",
                redirectPath = "/")
    }

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnNoHeadingSlashApiPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = "api/shorten/",
                redirectPath = "/")
    }

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnNoTrailingSlashApiPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = "/api/shorten",
                redirectPath = "/")
    }

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnBlankRedirectPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = "/api/shorten/",
                redirectPath = " \t\n")
    }

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnNoHeadingSlashRedirectPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = "/api/shorten/",
                redirectPath = "redirect/")
    }

    @Test(expected = IllegalArgumentException::class)
    fun validationFailsOnNoTrailingSlashRedirectPath() {
        UrlShortener(
                shortener = mock(Shortener::class.java),
                storageProvider = mock(StorageProvider::class.java),
                apiPath = "/api/shorten/",
                redirectPath = "/redirect")
    }
}
