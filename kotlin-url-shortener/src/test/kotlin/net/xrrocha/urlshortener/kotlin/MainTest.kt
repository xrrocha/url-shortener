package net.xrrocha.urlshortener.kotlin

import com.nhaarman.mockito_kotlin.*
import junit.framework.TestCase.assertEquals
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.security.Permission

/**
 * Unit tests for Main.
 */
class MainTest {

    companion object {
        /**
         * A custom <code>SecurityManager</code> is instated that traps and ignores
         * invocations to <code>System.exit(1)</code>. The system security manager
         * is reinstated after running all tests.
         */
        private val systemSecurityManager = System.getSecurityManager()

        private val logger: Logger by lazy { LoggerFactory.getLogger(this::class.java.name) }

        private object mainSecurityManager : SecurityManager() {
            override fun checkPermission(permission: Permission) {
                if ("exitVM.1" == permission.name) {
                    throw SecurityException("System.exit() blocked.")
                }
            }
        }

        @BeforeClass
        @JvmStatic
        fun setSecurityManager() {
            logger.info("Setting main security manager")
            System.setSecurityManager(mainSecurityManager)
        }

        @AfterClass
        @JvmStatic
        fun restoreSecurityManager() {
            logger.info("Setting system security manager")
            System.setSecurityManager(systemSecurityManager)
        }
    }

    @Test
    fun usesDefaultFilenameOnNoArgs() {
        val filename = Main.configFromArgs(arrayOf())
        assertEquals(Main.DEFAULT_CONFIG_NAME, filename)
    }

    @Test
    fun returnsFilenameWithOneArg() {
        val expectedFilename = "expected.yaml"
        val actualFilename = Main.configFromArgs(arrayOf(expectedFilename))
        assertEquals(expectedFilename, actualFilename)
    }

    @Test(expected = SecurityException::class)
    fun exitsWhenMoreThanOneArg() {
        Main.configFromArgs(arrayOf("one.yaml", "two.yaml"))
    }

    @Test(expected = SecurityException::class)
    fun exitsOnNonExistentResource() {
        Main.openConfigFile("non-existent.yaml")
    }

    @Test(expected = SecurityException::class)
    fun exitsOnEmptyYaml() {
        val inputStream = Main.openConfigFile("empty.yaml")
        Main.buildUrlShortener(inputStream, "empty.yaml")
    }

    @Test(expected = SecurityException::class)
    fun exitsOnBadYaml() {
        val inputStream = Main.openConfigFile("bad.yaml")
        Main.buildUrlShortener(inputStream, "bad.yaml")
    }

    @Test
    fun returnsDefaultResourceNameOnNoArg() {
        assertEquals(Main.DEFAULT_CONFIG_NAME, Main.configFromArgs(arrayOf()))
    }

    @Test
    fun returnsResourceNameOnOneArg() {
        val expectedResourceName = "explicit-filename.yaml"
        assertEquals(expectedResourceName, Main.configFromArgs(arrayOf(expectedResourceName)))
    }

    @Test(expected = SecurityException::class)
    fun exitsOnMoreThanOneArg() {
        Main.configFromArgs(arrayOf("param1", "param2"))
    }

    @Ignore
    @Test(expected = SecurityException::class)
    fun exitsOnUrlShortenerStartFailure() {
        val urlShortener = spy<UrlShortener>()
        whenever(urlShortener.start()).doThrow(IllegalStateException("Oops!"))
        Main.startUrlShortener(urlShortener)
    }

}