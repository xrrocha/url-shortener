package net.xrrocha.urlshortener.kotlin.shortening

import junit.framework.TestCase.assertEquals
import org.junit.Test

/**
 * Murmur3 shortening unit tests.
 */
class Murmur3ShortenerTest {

    @Test
    fun shortensProperly() {
        val murmur3Shortener = Murmur3Shortener()
        val shortenedString = murmur3Shortener.shorten("ratherLongValueInNeedOfShortening")
        assertEquals("c8293269", shortenedString)
    }
}
