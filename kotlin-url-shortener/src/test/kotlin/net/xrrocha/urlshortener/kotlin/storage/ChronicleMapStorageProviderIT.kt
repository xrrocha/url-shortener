package net.xrrocha.urlshortener.kotlin.storage

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.Closeable
import java.io.File
import java.io.IOException

/**
 * ChronicleMap storage provider unit test.
 */
class ChronicleMapStorageProviderIT {
    @Test
    @Throws(IOException::class)
    fun persistsDataAcrossClosings() {
        val file = File.createTempFile("data", ".tmp")
        file.deleteOnExit()

        val provider = ChronicleMapStorageProvider(
                name = "test",
                filename = file.absolutePath,
                entries = 16L,
                averageKeySize = 8.0,
                averageValueSize = 32.0)

        val firstStorage = provider.openStorage()
        (firstStorage as Closeable).use { _ ->
            assertTrue(firstStorage.isEmpty())
            firstStorage.put("short", "long")
            assertEquals("long", firstStorage.get("short"))
        }

        val secondStorage = provider.openStorage()
        (secondStorage as Closeable).use { _ ->
            assertTrue(secondStorage.size == 1)
            assertEquals("long", secondStorage.get("short"))
        }
    }
}
