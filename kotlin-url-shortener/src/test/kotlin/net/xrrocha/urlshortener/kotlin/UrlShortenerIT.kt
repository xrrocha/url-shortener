package net.xrrocha.urlshortener.kotlin

import junit.framework.TestCase.assertEquals
import net.xrrocha.urlshortener.kotlin.shortening.Shortener
import net.xrrocha.urlshortener.kotlin.storage.StorageProvider
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus.*
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultRedirectStrategy
import org.apache.http.impl.client.HttpClientBuilder
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import org.mockito.Mockito
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.Spark.awaitInitialization
import spark.Spark.get
import java.io.ByteArrayOutputStream
import java.io.Closeable
import java.io.File

/**
 * Integration test for Main. Unit tests in this suite are run in sequence
 * and may fail if run individually.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UrlShortenerIT {

    companion object {
        private val BASE_URL = "http://localhost:4567"
        private val API_PREFIX = "/api/shorten/"
        private val API_URL = BASE_URL + API_PREFIX
        private val REDIRECT_URL = BASE_URL + "/"

        private val TEST_PATH =
                "/test/ridiculously/long/ugly/path/longUglyFilename.looong"

        private val TEST_SHORT_PATH = "770ae8b1"
        private val TEST_URL = BASE_URL + TEST_PATH
        private val EXPECTED_SHORT_URL = REDIRECT_URL + TEST_SHORT_PATH

        private val EXPECTED_TEST_RESPONSE =
                "Content coming from ridiculously long URL. Not so long itself, though"

        private val dataFile = File("target/url-shortening.dat")

        private val httpClient =
                HttpClientBuilder
                        .create()
                        .setRedirectStrategy(DefaultRedirectStrategy())
                        .build()

        private val inputStream = Main.openConfigFile(Main.DEFAULT_CONFIG_NAME)
        private val urlShortener = Main.buildUrlShortener(inputStream, Main.DEFAULT_CONFIG_NAME)

        val logger: Logger by lazy { LoggerFactory.getLogger(this::class.java.name) }

        @BeforeClass
        @JvmStatic
        fun startup() {
            logger.debug("Starting SparkJava server")
            dataFile.delete()

            urlShortener.start()
            get("/test/*") { _, _ -> EXPECTED_TEST_RESPONSE }

            awaitInitialization()
        }

        @AfterClass
        @JvmStatic
        fun shutdown() {
            logger.debug("Stopping SparkJava server")
            urlShortener.stop()
        }

        private fun doPost(apiUrl: String, longUrl: String): HttpResponse {
            logger.debug("doPost($apiUrl, $longUrl)")
            val request = HttpPost(apiUrl)
            val httpEntity = StringEntity(longUrl)
            request.entity = httpEntity
            return httpClient.execute(request)
        }

        private fun doGet(shortUrl: String): HttpResponse {
            logger.debug("doGet($shortUrl)")
            val request = HttpGet(shortUrl)
            return httpClient.execute(request)
        }

        private fun doDelete(shortUrl: String): HttpResponse {
            logger.debug("doDelete($shortUrl)")
            val request = HttpDelete(shortUrl)
            return httpClient.execute(request)
        }


        private fun getBody(entity: HttpEntity): String {
            val baos = ByteArrayOutputStream()
            entity.writeTo(baos)
            val body = baos.toString()
            logger.debug("getBody($body)")
            return body
        }
    }

    @Test
    fun s1_shortensValidLongUrl() {
        val response = doPost(API_URL, TEST_URL)

        assertEquals(SC_CREATED, response.statusLine.statusCode)

        val actualShortUrl = getBody(response.entity)
        assertEquals(EXPECTED_SHORT_URL, actualShortUrl)
    }

    @Test
    @Throws(Exception::class)
    fun s2_failsOnInvalidLongUrl() {
        val invalidLongUrl = "invalidUrl"
        val response = doPost(API_URL, invalidLongUrl)
        assertEquals(SC_BAD_REQUEST, response.statusLine.statusCode)
    }

    @Test
    @Throws(Exception::class)
    fun s3_redirectsOnValidShortUrl() {
        val response = doGet(EXPECTED_SHORT_URL)
        assertEquals(SC_OK, response.statusLine.statusCode)
        assertEquals(EXPECTED_TEST_RESPONSE, getBody(response.entity))
    }

    @Test
    @Throws(Exception::class)
    fun s4_deletesExistingShortUrl() {
        val response = doDelete(EXPECTED_SHORT_URL)
        assertEquals(SC_NO_CONTENT, response.statusLine.statusCode)
    }

    @Test
    @Throws(Exception::class)
    fun s5_failsDeletionOnNonExistingShortUrl() {
        val response = doDelete(EXPECTED_SHORT_URL)
        assertEquals(SC_NOT_FOUND, response.statusLine.statusCode)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun s6_closesComponentsOnStop() {
        val closeableMap = Mockito.mock(Map::class.java,
                Mockito.withSettings().extraInterfaces(Closeable::class.java))

        val storageProvider = Mockito.mock(StorageProvider::class.java)
        Mockito.`when`(storageProvider.openStorage())
                .thenReturn(closeableMap as MutableMap<String, String>)

        val urlShortener = UrlShortener(
                shortener = Mockito.mock(Shortener::class.java),
                storageProvider = storageProvider,
                apiPath = "/api/shorten/",
                redirectPath = "/")

        urlShortener.start()
        urlShortener.stop()

        Mockito.verify(closeableMap as Closeable, Mockito.times(1)).close()
    }

}