# URL Shortener in Java, Kotlin and Xtend

This repository contains a simple -but functional- URL shortener implemented
in Java, Kotlin and Xtend. The accompanying documentation is available as a
[series of articles](https://xrrocha.net/post/url-shortener-1) on
[xrrocha.net](https://xrrocha.net). These articles cover the service design, its implementation in
Java and step-by-step explanations of its Kotlin and Xtend implementations. These explanations contrast Kotlin, Xtend and Java in order to introduce these JVM language to Java developers.

[Kotlin](https://kotlinlang.org/) is a modern JVM language created by Jetbrains (of Intellij Idea fame). Kotlin features many advanced constructs for functional programming as well as transparent integration with Java.

[Xtend](http://eclipse.org/xtend) is a JVM language created by the Eclipse Foundation. Unlike most other JVM languages, Xtend compiles into readable Java code and is fully compatible with all Java frameworks, libraries and tools. If you know Java you already know most of Xtend!



