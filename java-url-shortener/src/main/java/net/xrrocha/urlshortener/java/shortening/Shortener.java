package net.xrrocha.urlshortener.java.shortening;

/**
 * String shortening.
 */
public interface Shortener {
  /**
   * Shorten a string by applying an appropriate hash function so as to
   * minimize the possibility of collisions.
   * @param string The string to be shortened
   * @return The shortened string
   */
  String shorten(String string);
}
