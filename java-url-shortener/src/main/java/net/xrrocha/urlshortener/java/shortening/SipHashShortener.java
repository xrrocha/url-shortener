package net.xrrocha.urlshortener.java.shortening;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * SipHash-based shortening. This class only supports UTF-8.
 */
public class SipHashShortener implements Shortener {
  @Override
  public String shorten(String string) {
    checkNotNull(string, "String to be shortened cannot be null");
    return Hashing.sipHash24().
      hashString(string, StandardCharsets.UTF_8).
      toString();
  }
}
