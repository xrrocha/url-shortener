package net.xrrocha.urlshortener.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * <p>Application to build and activate a RESTful <code>UrlShortener</code>
 * instance.</p>
 * <p>A ready-made <code>UrlShortener</code> is created from a Yaml resource
 * file. This fileis expect to contain the full instantiation of a URL
 * shortening together with all its dependencies.</p>
 */
public class Main {

  /**
   * The default resource Yaml file to use if not given as a command-line
   * argument.
   */
  static final String DEFAULT_CONFIG_NAME = "url-shortener.yaml";

  private static Logger logger = LoggerFactory.getLogger(Main.class);

  /**
   * Build and start a <code>UrlShortener</code> instance from a resource
   * Yaml file. A shutdown hook is created to give the URL shortening instance
   * the opportunity to gracefully terminate any disposable dependencies it
   * may have.
   *
   * @param args
   */
  public static void main(String... args) {
    // Extract configuration filename from CLI arguments
    final String configFilename = configFromArgs(args);
    logger.debug("Configuration file: {}", configFilename);

    final InputStream in = openConfigFile(configFilename);

    final UrlShortener urlShortener = buildUrlShortener(in, configFilename);

    startUrlShortener(urlShortener);
  }

  /**
   * Start a newly created instance of <code>UrlShortener</code>. Failure to
   * start results in application exit. Adds hook to close the
   * <code>UrlShortener</code> gracefully upon JVM shutdown.
   *
   * @param urlShortener
   */
  static void startUrlShortener(UrlShortener urlShortener) {
    try {
      urlShortener.start();
      logger.info("REST server started");
    } catch (Exception e) {
      // Failure to start is associated with incorrect configuration or
      // misbehaving dependencies
      exit("Error starting URL shortening: " + e.getMessage());
    }

    // Make sure disposable dependencies are gracefully stopped upon JVM
    // shutdown
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      logger.info("Stopping URL shortening service");
      urlShortener.stop();
    }));
  }

  /**
   * Build a new instance of <code>UrlShortener</code> from a input stream
   * pointing to a Yaml configuration file.
   *
   * @param in The input stream associated with configuration file
   * @param filename The filename to use for error messages
   * @return The ready-made <code>UrlShortener</code> instance entirely
   * created by SnakeYaml.
   */
  static UrlShortener buildUrlShortener(InputStream in, String filename) {
    Yaml yaml = new Yaml();
    yaml.setBeanAccess(BeanAccess.FIELD);
    UrlShortener urlShortener = null;

    try {
      urlShortener = yaml.loadAs(in, UrlShortener.class);
    } catch (Exception e) {
      exit("Error in configuration file '" + filename + "': " + e);
    }

    if (urlShortener == null) {
      exit("Invalid yaml content in configuration file: " + filename);
    }

    return urlShortener;
  }

  /**
   * Opens the configuration file looking for it on the file systema and,
   * failing that, as a classpath resource.
   *
   * @param filename The configuration filename
   * @return The input stream associated with the file
   */
  static InputStream openConfigFile(String filename) {
    InputStream is = null;

    if (new File(filename).exists()) {
      try {
        is = new FileInputStream(filename);
      } catch (FileNotFoundException e) {
        exit("Error opening configuration file '" + filename + "': " + e);
      }
    } else {
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      is = classLoader.getResourceAsStream(filename);
      if (is == null) {
        exit("Can't open configuration file: " + filename);
      }
    }

    return is;
  }

  /**
   * Extract the configuration resource file name from the command line
   * arguments.
   *
   * @param args The arguments passed to <code>main</code> from the command line
   * @return The configuration resource filename.
   */
  static String configFromArgs(String... args) {
    String configurationFilename = null;
    switch (args.length) {
      case 0:
        configurationFilename = DEFAULT_CONFIG_NAME;
        break;
      case 1:
        configurationFilename = args[0];
        break;
      default:
        String errorMessage = String.format(
          "Usage: %s <configYamlFile|%s>",
          Main.class.getName(), DEFAULT_CONFIG_NAME);
        exit(errorMessage);
    }

    return configurationFilename;
  }

  static void exit(String message) {
    System.err.println(message);
    System.exit(1);
  }

}
