package net.xrrocha.urlshortener.java.storage;

import net.openhft.chronicle.map.ChronicleMapBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * <p>ChronicleMap-based storage provider implementation. This class persists
 * key/value pairs on the filesystem. ChronicleMap files require to be closed
 * upon deactivation. Currently, no attempt is made to recover the persistent
 * file in case of abrupt shutdown.</p>
 */
public class ChronicleMapStorageProvider implements StorageProvider {
  /**
   * A free-form name globaly unique across all active ChronicleMaps on the
   * same JVM.
   */
  private String name;
  /**
   * The name of the file to be used as persitent store. Will be created if
   * not yet existent. Existent files may be corrupted if the associated map
   * is not properly
   * closed.
   */
  private String filename;
  /**
   * The effective maximum number of key/value pairs to be held in the map.
   */
  private int entries;
  /**
   * The average key size in bytes.
   */
  private double averageKeySize;
  /**
   * The average value size in bytes.
   */
  private double averageValueSize;

  /**
   * Private default constructor for SnakeYAML to instantiate
   */
  private ChronicleMapStorageProvider() {
  }

  public ChronicleMapStorageProvider(String name,
                                     String filename,
                                     int entries,
                                     double averageKeySize,
                                     double averageValueSize) {
    this.name = name;
    this.filename = filename;
    this.entries = entries;
    this.averageKeySize = averageKeySize;
    this.averageValueSize = averageValueSize;
    
    validate();
  }

  @Override
  /**
   * Build a new ChronicleMap instance with the given configuration.
   * @return the newly created <code>Map&lt;String, String&gt;</code> backed
   * by a file store.
   */
  public Map<String, String> openStorage() {
    try {
      return ChronicleMapBuilder.
        of(String.class, String.class).
        name(this.name).
        entries(this.entries).
        averageKeySize(this.averageKeySize).
        averageValueSize(this.averageValueSize).
        createPersistedTo(new File(filename));
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  void validate() {
    checkArgument(name != null && name.trim().length() > 0,
      "Name cannot be null or blank");
    checkArgument(filename != null && filename.trim().length() > 0,
      "Name cannot be null or blank");
    checkArgument(entries  > 0d,
      "Entries cannot be zero or negative");
    checkArgument(averageKeySize  > 0d,
      "Average key size cannot be zero or negative");
    checkArgument(averageValueSize  > 0d,
      "Average value size cannot be zero or negative");
  }
  
  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private String name;
    private String filename;
    private int entries;
    private double averageKeySize;
    private double averageValueSize;

    private Builder() {
    }

    public static Builder aChronicleMapStorageProvider() {
      return new Builder();
    }

    public Builder withName(String name) {
      this.name = name;
      return this;
    }

    public Builder withFilename(String filename) {
      this.filename = filename;
      return this;
    }

    public Builder withEntries(int entries) {
      this.entries = entries;
      return this;
    }

    public Builder withAverageKeySize(double averageKeySize) {
      this.averageKeySize = averageKeySize;
      return this;
    }

    public Builder withAverageValueSize(double averageValueSize) {
      this.averageValueSize = averageValueSize;
      return this;
    }

    public ChronicleMapStorageProvider build() {
      ChronicleMapStorageProvider chronicleMapStorageProvider =
        new ChronicleMapStorageProvider(
          name, filename, entries, averageKeySize, averageValueSize);
      return chronicleMapStorageProvider;
    }
  }
}
