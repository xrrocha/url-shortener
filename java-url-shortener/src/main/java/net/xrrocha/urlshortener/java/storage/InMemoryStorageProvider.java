package net.xrrocha.urlshortener.java.storage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A simple, in-memory storage provider returning a concurrent hash map.
 */
public class InMemoryStorageProvider implements StorageProvider {
  @Override
  /**
   * Return a new concurrent hash map.
   * @return the newly created in-memory map.
   */
  public Map<String, String> openStorage() {
    return new ConcurrentHashMap<>();
  }
}
