package net.xrrocha.urlshortener.java;

import net.xrrocha.urlshortener.java.shortening.Shortener;
import net.xrrocha.urlshortener.java.storage.StorageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Response;
import spark.Service;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;

/**
 * <p>HTTP service exposing URL shortening operations through a REST API.
 * This class is the workhorse of the URL shoertening service.</p>
 * <p>The API URL can be configured through setter and defaults to
 * "/api/v1/shorten"</p>
 * <p>This class has dependencies on two interface types:</p>
 * <ul>
 * <li><code>Shortener</code>: the component in charge of computing a short
 * hash from a given URL.</li>
 * <li><code>StorageProvider</code>: the <code>Map&lt;String, String&gt;
 * </code> storage where shortUrl/longUrl key/value pairs are stored. This
 * map will typically be persisted.</li>
 * </ul>
 */
public class UrlShortener {
  /**
   * The component in charge of deriving a short hash from a given URL.
   */
  Shortener shortener;
  /**
   * The component in charge of providing a key/value store exposed as a
   * <code>Map&lt;String, String&gt;</code>.
   */
  StorageProvider storageProvider;
  /**
   * The API path prefix to be used for REST endpoints.
   */
  String apiPath;
  /**
   * The redirect path prefix to be used for retrieving short URL's.
   */
  String redirectPath;
  /**
   * The webserver port number
   */
  int portNumber;
  /**
   * The threadpool size
   */
  int threadPoolSize;

  /**
   * The SparkJava service created from configuration
   */
  Service service;

  /**
   * The <code>Map&lt;String, String&gt;</code> used to store
   * shortUrl/longUrl key/value pairs. This map is drawn once -at startup-
   * from the <code>StorageProvider</code> dependency.
   */
  Map<String, String> storage;

  /**
   * The logger instance
   */
  private static Logger logger = LoggerFactory.getLogger(UrlShortener.class);

  private UrlShortener() {
  }

  public UrlShortener(Shortener shortener,
                      StorageProvider storageProvider,
                      String apiPath,
                      String redirectPath,
                      int portNumber,
                      int threadPoolSize) {
    this.shortener = shortener;
    this.storageProvider = storageProvider;
    this.apiPath = apiPath;
    this.redirectPath = redirectPath;
    this.portNumber = portNumber;
    this.threadPoolSize = threadPoolSize;
    validate();
  }

  /**
   * <p>Implicitly start a <em>default</em> <code>Spark</code> service
   * instance by defining all the available REST endpoints:</p>
   * <ul>
   * <li>
   * <code>POST /api/v1/shorten</code>: Shorten URL passed as POST body
   * </li>
   * <li>
   * <code>GET /api/v1/shorten/:shortUrl</code>: Redirect to long URL based
   * on the short
   * URL requested as GET
   * </li>
   * <li>
   * <code>DELETE /api/v1/shorten/:shortUrl</code>: Remove short/long URL
   * pair given
   * by :shortUrl
   * </li>
   * </ul>
   * <p>The content type is <code>text/plain</code> for all endpoints</p>
   */
  public void start() {
    logger.info("Starting UrlShortener service");

    // May have been initialized reflectively
    validate();

    storage = storageProvider.openStorage();
    if (storage == null) {
      String errorMessage = "Storage provider yielded null storage";
      logger.error(errorMessage);
      throw new NullPointerException(errorMessage);
    }

    service = Service.ignite().port(portNumber).threadPool(threadPoolSize);

    /*
     * Shorten and store a long url passed as POST body. The returned entity
     * body contains the shortened URL to be used in lieu of the input long one.
     */
    logger.debug("Configuring POST({})", apiPath);
    service.post(apiPath, (req, res) -> {
      final URI uri;
      String longUrl = req.body();
      try {
        uri = new URI(longUrl);
      } catch (Exception e) {
        res.status(SC_BAD_REQUEST);
        return "Malformed URL: " + longUrl;
      }

      if (uri.getScheme() != null && uri.getScheme().startsWith("http")) {
        String hash = shortener.shorten(longUrl);
        storage.put(hash, longUrl);
        String redirectUrl =
          new URI(req.url()).resolve(redirectPath + hash).toString();
        res.status(SC_CREATED);
        return redirectUrl;
      } else {
        return completeResponse(res, SC_BAD_REQUEST, "Not an HTTP URL: " + longUrl);
      }
    });

    /*
     * Expand short URL (pointed to by :hash) and redirect to it
     */
    logger.debug("Configuring GET({})", redirectPath + ":hash");
    service.get(redirectPath + ":hash", (req, res) -> {
      String hash = req.params(":hash");
      if (storage.containsKey(hash)) {
        String longUrl = storage.get(hash);
        res.redirect(longUrl);
        return "";
      } else {
        return
          completeResponse(res,
            SC_NOT_FOUND, "No such short URL: " + req.url());
      }
    });

    /*
     * Delete short/long URL pair given by :hash
     */
    logger.debug("Configuring DELETE({})", redirectPath + ":hash");
    service.delete(redirectPath + ":hash", (req, res) -> {
      String hash = req.params(":hash");
      if (storage.containsKey(hash)) {
        storage.remove(hash);
        res.status(SC_NO_CONTENT);
      } else {
        res.status(SC_NOT_FOUND);
      }
      return "";
    });
  }

  /**
   * Stop the Spark default service and close the shortening and/or storage if
   * they implement <code>Closeable</code>.
   */
  public void stop() {
    logger.info("Stopping UrlShortener service");
    if (service != null) {
      service.stop();
      tryAndClose(storage);
    }
  }

  /**
   * Validate configuration and dependencies prior to starting Spark default
   * service.
   */
  void validate() {
    checkArgument(apiPath != null && apiPath.trim().length() > 0,
      "API path cannot be null or blank");
    checkArgument(
      apiPath.startsWith("/"),
      "API path must start with slash");
    checkArgument(
      redirectPath != null && redirectPath.trim().length() > 0,
      "Redirect path cannot be null or blank");
    checkArgument(
      redirectPath.startsWith("/") && redirectPath.endsWith("/"),
      "Redirect path must start and end with slash");
    checkNotNull(shortener, "Shortener cannot be null");
    checkNotNull(storageProvider, "Storage provider cannot be null");
    checkArgument(portNumber > 1024, "Invalid port number: " + portNumber);
  }

  /**
   * Complete response returning a string body
   *
   * @param response The response to be completed
   * @param status   The Http status
   * @return An empty request body
   */
  String completeResponse(Response response, int status) {
    return completeResponse(response, status, "");
  }

  /**
   * Complete response returning a string body
   *
   * @param response The response to be completed
   * @param status   The Http status
   * @return The request body string
   */
  String completeResponse(Response response, int status, String body) {
    response.status(status);
    return body;
  }

  /**
   * Try and close and object if it implements <code>Closeable</code>
   *
   * @param obj The object to be closed if needed
   */
  static void tryAndClose(Object obj) {
    if (obj instanceof Closeable) {
      try {
        ((Closeable) obj).close();
      } catch (IOException e) {
        logger.warn("Ignoring error closing component: {}", (Object) e);
      }
    }
  }

  public static Builder builder() {
    return new Builder();
  }

  public Builder copy() {
    return new Builder(this);
  }

  public static final class Builder {
    private Shortener shortener;
    private StorageProvider storageProvider;
    private String apiPath;
    private String redirectPath;
    private int portNumber;
    private int threadPoolSize;

    private Builder() {
    }

    private Builder(UrlShortener urlShortener) {
      this.shortener = urlShortener.shortener;
      this.storageProvider = urlShortener.storageProvider;
      this.apiPath = urlShortener.apiPath;
      this.redirectPath = urlShortener.redirectPath;
      this.portNumber = urlShortener.portNumber;
      this.threadPoolSize = urlShortener.threadPoolSize;
    }

    public Builder withShortener(Shortener shortener) {
      this.shortener = shortener;
      return this;
    }

    public Builder withStorageProvider(StorageProvider storageProvider) {
      this.storageProvider = storageProvider;
      return this;
    }

    public Builder withApiPath(String apiPath) {
      this.apiPath = apiPath;
      return this;
    }

    public Builder withRedirectPath(String redirectPath) {
      this.redirectPath = redirectPath;
      return this;
    }

    public Builder withPortNumber(int portNumber) {
      this.portNumber = portNumber;
      return this;
    }

    public Builder withThreadPoolSize(int threadPoolSize) {
      this.threadPoolSize = threadPoolSize;
      return this;
    }

    public UrlShortener build() {
      return new
        UrlShortener(shortener, storageProvider, apiPath, redirectPath, portNumber, threadPoolSize);
    }
  }
}
