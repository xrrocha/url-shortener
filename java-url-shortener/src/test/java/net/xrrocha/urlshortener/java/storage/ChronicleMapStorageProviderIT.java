package net.xrrocha.urlshortener.java.storage;

import org.junit.Test;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * ChronicleMap storage provider unit test.
 */
public class ChronicleMapStorageProviderIT {
  @Test
  public void persistsDataAcrossClosings() throws IOException {
    File file = File.createTempFile("data", ".tmp");
    file.deleteOnExit();

    ChronicleMapStorageProvider provider =
      ChronicleMapStorageProvider.builder().
        withName("test").
        withFilename(file.getAbsolutePath()).
        withEntries(16).
        withAverageKeySize(8).
        withAverageValueSize(32).
        build();

    Map<String, String> firstStorage = provider.openStorage();
    try (Closeable storage = (Closeable) firstStorage) {
      assertTrue(firstStorage.isEmpty());
      firstStorage.put("short", "long");
      assertEquals("long", firstStorage.get("short"));
    }

    Map<String, String> secondStorage = provider.openStorage();
    try(Closeable storage = (Closeable) secondStorage) {
      assertTrue(secondStorage.size() == 1);
      assertEquals("long", secondStorage.get("short"));
    }
  }

  @Test(expected = NullPointerException.class)
  public void failsOnNullKey() throws Exception {
    File file = File.createTempFile("data", ".tmp");
    file.deleteOnExit();

    ChronicleMapStorageProvider provider =
      ChronicleMapStorageProvider.builder().
        withName("test").
        withFilename(file.getAbsolutePath()).
        withEntries(16).
        withAverageKeySize(8).
        withAverageValueSize(32).
        build();

    Map<String, String> storage = provider.openStorage();
    try(Closeable c = (Closeable) storage) {
      storage.put(null, "value");
    }
  }

  @Test(expected = NullPointerException.class)
  public void failsOnNullValue() throws Exception {
    File file = File.createTempFile("data", ".tmp");
    file.deleteOnExit();

    ChronicleMapStorageProvider provider =
      ChronicleMapStorageProvider.builder().
        withName("test").
        withFilename(file.getAbsolutePath()).
        withEntries(16).
        withAverageKeySize(8).
        withAverageValueSize(32).
        build();

    Map<String, String> storage = provider.openStorage();
    try(Closeable c = (Closeable) storage) {
      storage.put("key", null);
    }
  }

}
