package net.xrrocha.urlshortener.java.storage;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * InMemoryStorage unit tests.
 */
public class InMemoryStorageProviderTest {
  @Test
  public void returnsProperMap() {
    InMemoryStorageProvider provider = new InMemoryStorageProvider();
    Map<String, String> storage = provider.openStorage();
    assertTrue(storage.isEmpty());
    storage.put("short", "long");
    assertEquals("long", storage.get("short"));
  }
}
