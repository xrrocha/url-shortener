package net.xrrocha.urlshortener.java.storage;

import org.junit.Test;

/**
 * Unit tests for ChronicleMap storage provider.
 */
public class ChronicleMapStorageProviderTest {

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnNullName() {
    ChronicleMapStorageProvider.builder().
      withName(null).
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnEmptyName() {
    ChronicleMapStorageProvider.builder().
      withName("").
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnBlankName() {
    ChronicleMapStorageProvider.builder().
      withName(" \t\n").
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnNullFilename() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename(null).
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnEmptyFilename() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnBlankFilename() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename(" \t\n").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnZeroEntries() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("/tmp/keyValue.dat").
      withEntries(0).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnNegativeEntries() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("/tmp/keyValue.dat").
      withEntries(-1).
      withAverageKeySize(32).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnZeroAverageKeySize() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(0).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnNegativeAverageKeySize() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(-1).
      withAverageValueSize(64).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnZeroAverageValueSize() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(0).
      build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void failsValidationOnNegativeAverageValueSize() {
    ChronicleMapStorageProvider.builder().
      withName("someName").
      withFilename("/tmp/keyValue.dat").
      withEntries(1048576).
      withAverageKeySize(32).
      withAverageValueSize(-1).
      build();
  }
}
