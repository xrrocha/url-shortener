package net.xrrocha.urlshortener.java.shortening;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Murmur3 shortening unit tests.
 */
public class Murmur3ShortenerTest {
  @Test(expected = NullPointerException.class)
  public void failsOnNullString() {
    Murmur3Shortener murmur3Shortener = new Murmur3Shortener();
    murmur3Shortener.shorten(null);
  }

  @Test
  public void shortensProperly() {
    Murmur3Shortener murmur3Shortener = new Murmur3Shortener();
    String shortenedString =
      murmur3Shortener.shorten("ratherLongValueInNeedOfShortening");
    assertEquals("c8293269", shortenedString);
  }
}
