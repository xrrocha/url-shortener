package net.xrrocha.urlshortener.java;

import net.xrrocha.urlshortener.java.Main;
import net.xrrocha.urlshortener.java.UrlShortener;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.security.Permission;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

/**
 * Unit tests for Main.
 */
public class MainTest {
  /**
   * A custom <code>SecurityManager</code> is instated that traps and ignores
   * invocations to <code>System.exit(1)</code>. The system security manager
   * is reinstated after running all tests.
   */
  private static final SecurityManager
    systemSecurityManager = System.getSecurityManager();

  private static final SecurityManager mainSecurityManager =
    new SecurityManager() {
      @Override
      public void checkPermission(Permission permission) {
        if ("exitVM.1".equals(permission.getName())) {
          throw new SecurityException("System.exit() blocked.");
        }
      }
    };

  @BeforeClass
  public static void setSecurityManager() {
    System.setSecurityManager(mainSecurityManager);
  }

  @AfterClass
  public static void restoreSecurityManager() {
    System.setSecurityManager(systemSecurityManager);
  }

  @Test
  public void usesDefaultFilenameOnNoArgs() {
    String filename = Main.configFromArgs();
    assertEquals(Main.DEFAULT_CONFIG_NAME, filename);
  }

  @Test
  public void returnsFilenameWithOneArg() {
    String expectedFilename = "expected.yaml";
    String actualFilename = Main.configFromArgs
      (expectedFilename);
    assertEquals(expectedFilename, actualFilename);
  }

  @Test(expected = SecurityException.class)
  public void exitsWhenMoreThanOneArg() {
    Main.configFromArgs("one.yaml", "two.yaml");
  }

  @Test(expected = SecurityException.class)
  public void exitsOnNonExistentResource() {
    Main.openConfigFile("non-existent.yaml");
  }

  @Test(expected = SecurityException.class)
  public void exitsOnEmptyYaml() {
    InputStream is = Main.openConfigFile("empty.yaml");
    Main.buildUrlShortener(is, "empty.yaml");
  }

  @Test(expected = SecurityException.class)
  public void exitsOnBadYaml() {
    InputStream is = Main.openConfigFile("bad.yaml");
    Main.buildUrlShortener(is, "bad.yaml");
  }

  @Test
  public void returnsDefaultResourceNameOnNoArg() {
    assertEquals(Main.DEFAULT_CONFIG_NAME,
      Main.configFromArgs());
  }

  @Test
  public void returnsResourceNameOnOneArg() {
    String expectedResourceName = "explicit-filename.yaml";
    assertEquals(expectedResourceName, Main.configFromArgs
      (expectedResourceName));
  }

  @Test(expected = SecurityException.class)
  public void exitsOnMoreThanOneArg() {
    Main.configFromArgs("param1", "param2");
  }

  @Test(expected = SecurityException.class)
  public void exitsOnUrlShortenerStartFailure() {
    UrlShortener urlShortener = mock(UrlShortener.class);
    doThrow(new IllegalStateException("Oops!")).when(urlShortener).start();
    Main.startUrlShortener(urlShortener);
  }
}
