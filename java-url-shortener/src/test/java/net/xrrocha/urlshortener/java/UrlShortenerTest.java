package net.xrrocha.urlshortener.java;

import net.xrrocha.urlshortener.java.shortening.Shortener;
import net.xrrocha.urlshortener.java.storage.StorageProvider;
import org.junit.Test;

import java.io.Closeable;
import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * URL shortening unit tests.
 */
public class UrlShortenerTest {
  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnNullApiPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath(null)
      .withRedirectPath("/")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnEmptyApiPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("")
      .withRedirectPath("/")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnBlankApiPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath(" \t\n")
      .withRedirectPath("/")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnNoSlashApiPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("api/shorten")
      .withRedirectPath("/")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnNullRedirectPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("/api/shorten")
      .withRedirectPath(null)
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnEmptyRedirectPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("/api/shorten")
      .withRedirectPath("")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnBlankRedirectPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("/api/shorten")
      .withRedirectPath(" \t\n")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnNoHeadingSlashRedirectPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("/api/shorten")
      .withRedirectPath("redirect/")
      .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void validationFailsOnNoTrailingSlashRedirectPath() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("/api/shorten")
      .withRedirectPath("/redirect")
      .build();
  }

  @Test(expected = NullPointerException.class)
  public void validationFailsOnNullShortener() {
    UrlShortener.builder()
      .withShortener(null)
      .withStorageProvider(mock(StorageProvider.class))
      .withApiPath("/api/shorten")
      .withRedirectPath("/")
      .build();
  }

  @Test(expected = NullPointerException.class)
  public void validationFailsOnNullStorageProvider() {
    UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(null)
      .withApiPath("/api/shorten")
      .withRedirectPath("/")
      .build();
  }

  @Test(expected = NullPointerException.class)
  public void startFailsOnNullStorage() {
    StorageProvider storageProvider = mock(StorageProvider.class);
    when(storageProvider.openStorage()).thenReturn(null);
    UrlShortener urlShortener = UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(null)
      .withApiPath("/api/shorten")
      .withRedirectPath("/")
      .build();
    urlShortener.start();
  }

  @Test
  public void closesCloseableComponents() throws IOException {
    Closeable closeable = mock(Closeable.class);
    UrlShortener.tryAndClose(closeable);
    verify(closeable, times(1)).close();
  }

  @Test
  public void ignoresClosingException() throws IOException {
    Closeable closeable = mock(Closeable.class);
    doThrow(new IOException("Oops!")).when(closeable).close();
    UrlShortener.tryAndClose(closeable);
    verify(closeable, times(1)).close();
  }
}
