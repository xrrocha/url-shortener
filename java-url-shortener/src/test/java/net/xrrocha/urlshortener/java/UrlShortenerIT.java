package net.xrrocha.urlshortener.java;

import net.xrrocha.urlshortener.java.shortening.Murmur3Shortener;
import net.xrrocha.urlshortener.java.shortening.Shortener;
import net.xrrocha.urlshortener.java.storage.ChronicleMapStorageProvider;
import net.xrrocha.urlshortener.java.storage.StorageProvider;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Random;

import static junit.framework.TestCase.assertEquals;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

/**
 * Integration test for Main. Unit tests in this suite are run in sequence
 * and may fail if run individually.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UrlShortenerIT {

  private static final File chronicleMapFile = new File("target/url-shortening.dat");

  private static final UrlShortener urlShortener =
    UrlShortener.builder()
      .withApiPath("/api/shorten/")
      .withRedirectPath("/")
      .withPortNumber(randomPort())
      .withThreadPoolSize(16)
      .withShortener(new Murmur3Shortener())
      .withStorageProvider(
        ChronicleMapStorageProvider.builder()
          .withName("chronicleMac")
          .withFilename(chronicleMapFile.getName())
          .withEntries(1048576)
          .withAverageKeySize(32)
          .withAverageValueSize(64)
          .build())
      .build();

  private static final HttpClient httpClient =
    HttpClientBuilder
      .create()
      .setRedirectStrategy(new DefaultRedirectStrategy())
      .build();

  private static final String EXPECTED_TEST_RESPONSE =
    "Content coming from ridiculously long URL. Not so long itself, though";
  private static final String BASE_URL = "http://localhost:" + urlShortener.portNumber;
  private static final String API_URL = BASE_URL + urlShortener.apiPath;
  private static final String REDIRECT_URL = BASE_URL + urlShortener.redirectPath;

  private static final String TEST_PATH =
    "/test/ridiculously/long/ugly/path/longUglyFilename.looong";
  private static final String TEST_URL = BASE_URL + TEST_PATH;

  private static final Logger logger = LoggerFactory.getLogger(UrlShortenerIT.class);

  private static String resultingShortUrl;
  private static String resultingHash;

  @BeforeClass
  public static void startup() throws Exception {
    logger.debug("Starting SparkJava server on port {}", urlShortener.portNumber);
    chronicleMapFile.delete();

    urlShortener.start();
    urlShortener.service.get("/test/*", (req, res) -> EXPECTED_TEST_RESPONSE);

    urlShortener.service.awaitInitialization();
  }

  @AfterClass
  public static void shutdown() throws Exception {
    logger.debug("Stopping SparkJava server on port {}", urlShortener.portNumber);
    urlShortener.stop();
  }

  @Test
  public void s1_shortensValidLongUrl() throws IOException {
    HttpResponse response = doPost(API_URL, TEST_URL);

    assertEquals(SC_CREATED, response.getStatusLine().getStatusCode());

    resultingShortUrl = getBody(response.getEntity());
    resultingHash = pullHash(resultingShortUrl);
    logger.debug("resultingShortUrl: {}", resultingShortUrl);
    logger.debug("hash: {}", resultingHash);
    assertEquals(REDIRECT_URL + resultingHash, resultingShortUrl);
  }

  @Test
  public void s2_failsOnInvalidLongUrl() throws Exception {
    String invalidLongUrl = "invalidUrl";
    HttpResponse response = doPost(API_URL, invalidLongUrl);
    assertEquals(SC_BAD_REQUEST, response.getStatusLine().getStatusCode());
  }

  @Test
  public void s3_redirectsOnValidShortUrl() throws Exception {
    HttpResponse response = doGet(resultingShortUrl);
    assertEquals(SC_OK, response.getStatusLine().getStatusCode());
    assertEquals(EXPECTED_TEST_RESPONSE, getBody(response.getEntity()));
  }

  @Test
  public void s4_deletesExistingShortUrl() throws Exception {
    HttpResponse response = doDelete(resultingShortUrl);
    assertEquals(SC_NO_CONTENT, response.getStatusLine().getStatusCode());
  }

  @Test
  public void s5_failsDeletionOnNonExistingShortUrl() throws Exception {
    HttpResponse response = doDelete(resultingShortUrl);
    assertEquals(SC_NOT_FOUND, response.getStatusLine().getStatusCode());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void s6_closesComponentsOnStop() throws IOException {
    Map<String, String> closeableMap =
      mock(Map.class, withSettings().extraInterfaces(Closeable.class));

    StorageProvider storageProvider = mock(StorageProvider.class);
    when(storageProvider.openStorage()).thenReturn(closeableMap);

    UrlShortener urlShortener = UrlShortener.builder()
      .withShortener(mock(Shortener.class))
      .withStorageProvider(storageProvider)
      .withApiPath("/api/shorten")
      .withRedirectPath("/")
      .withPortNumber(randomPort())
      .withThreadPoolSize(16)
      .build();

    urlShortener.start();
    urlShortener.stop();

    verify((Closeable) closeableMap, times(1)).close();
  }

  private static HttpResponse doPost(String apiUrl, String longUrl) throws IOException {
    HttpPost request = new HttpPost(apiUrl);
    HttpEntity httpEntity = new StringEntity(longUrl);
    request.setEntity(httpEntity);
    return httpClient.execute(request);
  }

  private static HttpResponse doGet(String shortUrl) throws IOException {
    HttpGet request = new HttpGet(shortUrl);
    return httpClient.execute(request);
  }

  private static HttpResponse doDelete(String shortUrl) throws IOException {
    HttpDelete request = new HttpDelete(shortUrl);
    return httpClient.execute(request);
  }

  private static String getBody(HttpEntity entity) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    entity.writeTo(baos);
    String body = baos.toString();
    return body;
  }

  static int randomPort() {
    return 1024 + new Random(System.currentTimeMillis()).nextInt(32764);
  }

  static String pullHash(String shortUrl) {
    int startPos = shortUrl.lastIndexOf('/');
    return shortUrl.substring(startPos + 1);
  }
}
