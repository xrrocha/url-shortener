#!/bin/bash

# mvn shade:shade
# java -jar target/java-url-shortener-0.1.0-SNAPSHOT.jar src/test/resources/url-shortener.yaml

long_url="http://www.eclipse.org/xtend/documentation/202_xtend_classes_members.html#extension-methods"

short_url=$(curl \
    -X POST \
    -d "$long_url" \
    http://localhost:4567/api/shorten)

echo "Short URL: $short_url"

curl \
    -L \
    -X GET \
    $short_url > /tmp/url-shortener-result.html

wc /tmp/url-shortener-result.html
